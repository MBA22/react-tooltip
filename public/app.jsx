var ToolTip = React.createClass({
    getInitialState: function () {
        return ({
            opacity: false
        });
    },
    toggle: function () {
        var ToolTipNode = ReactDOM.findDOMNode(this);

        this.setState({
            opacity: !this.state.opacity,
            top: ToolTipNode.offsetTop,
            left: ToolTipNode.offsetLeft
        });
    },
    render: function () {

        var opac;
        var disp;
        if (this.state.opacity) {
            opac = 1000;
            disp = 'inline';
        }
        else {
            opac = -1000;
            disp = 'none';
        }
        var style = {

            zIndex: opac,
            display: disp,
            opacity: +this.state.opacity,

            top: (this.state.top || 0) + 10,

            left: (this.state.left || 0) - 10

        };

        return (

            <div style={{display: 'inline'}}>

        <span style={{color: '#AB2BB4'}} onMouseEnter={this.toggle} onMouseOut={this.toggle}>
          {this.props.children}
        </span>

                <div className="ToolTip bottom" style={style} role="ToolTip">

                    <div style={{display: 'inline'}} className="ToolTip-arrow"></div>

                    <div style={{display: 'inline'}} className="ToolTip-inner">

                        {this.props.text}

                    </div>

                </div>

            </div>
        );
    }
});

ReactDOM.render(
    <div className="container">
        <h1 className="text-center">Bruce Lee</h1>
        <ToolTip text="Lee Jun-fan, was an actor, martial artist, philosopher and founder of JKD">Bruce Lee </ToolTip>
        was
        the son of Cantonese opera star Lee Hoi-Chuen. He is widely considered by commentators, critics, media, and
        other
        martial artists to be one of the most influential martial artists of all time,and a pop culture icon of the 20th
        century.He is often credited with helping to change the way Asians were presented in American films.He was
        founder
        of <ToolTip
        text="Lee decided to develop a system with an emphasis on 'practicality, flexibility, speed, and efficiency'.">[Jeet
        Kune Do]</ToolTip> and it is one of the flourishing techniques in chinese martial arts .

    </div>, document.getElementById('app'));